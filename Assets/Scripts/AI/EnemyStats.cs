﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyStats
{
    public EnemyStats(int b_Health = 100, int dmg = 10)
    {
        baseHealth = b_Health;
        currHealth = b_Health;
        damage = dmg;
    }

    [SerializeField]
    private int baseHealth = 100;
    public int currHealth = 0;
    public int damage = 0;
    public bool isDead = false;

    public void Damage(int dmg)
    {
        if (damage < 0)
            isDead = true;
        currHealth -= dmg;
    }

    public void ResetHealth()
    {
        currHealth = baseHealth;
    }
}
