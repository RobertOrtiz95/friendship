﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public int damage = 0;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player p = collision.transform.GetComponent<Player>();
            p.m_CharacterController.rb2D.AddForce(p.transform.right * 25);
            p.Damage(damage);
        }
    }
}
