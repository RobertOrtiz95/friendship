﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonBehaviour : MonoBehaviour
{

    public EnemyStats dragonStats = new EnemyStats();
    public Transform player = null;
    public Collider2D bossBoundary = null;

    private Vector3 faceLeft = new Vector3(0,180,0);
    private Rigidbody2D rb2d = null;
    private Vector3 landingPos = Vector3.zero;
    
    private float idleTimer = 5;

    private void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private Vector2 GetPlayerPosition()
    {
        return player.position;
    }

    public bool MoveToWithPlayerTarget(Vector2 target, float speed)
    {
        if (target.x < transform.position.x)
            transform.rotation = Quaternion.Euler(faceLeft);
        else
            transform.rotation = Quaternion.Euler(Vector3.zero);

        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, target) < .1f)
            return true;

        return false;
    }

    public void DragonFlyToPos(Vector2 target, float speed)
    {
        if (target.x < transform.position.x)
            transform.rotation = Quaternion.Euler(faceLeft);
        else
            transform.rotation = Quaternion.Euler(Vector3.zero);

        rb2d.position = Vector3.Lerp(rb2d.position, target, speed * Time.deltaTime);
    }

    public bool DragonLand(float speed)
    {
        if (player.transform.position.x < transform.position.x)
            transform.rotation = Quaternion.Euler(faceLeft);
        else
            transform.rotation = Quaternion.Euler(Vector3.zero);

        landingPos = player.transform.position;
        landingPos.x += 3;

        transform.position = Vector3.MoveTowards(rb2d.position, landingPos, speed * Time.deltaTime);

        if (Vector3.Distance(landingPos, transform.position) < 1)
            return true;

        return false;
    }

    public bool Idle()
    {
        idleTimer += Time.deltaTime;
        if(idleTimer > 5)
        {
            idleTimer = Random.Range(1, 4);
            return true;
        }
        return false;
    }    

    public void UpdateLandingPos()
    {
        Vector3 pos = player.transform.position;
        pos.x += 2;
        landingPos = pos;
    }

    public bool CheckForPlayer()
    {
        Collider2D[] colls = Physics2D.OverlapBoxAll(bossBoundary.transform.position, bossBoundary.bounds.size, .5f);

        for(int i = 0; i < colls.Length; i++)
        {
            if(colls[i].tag == "Player")
            {
                player = colls[i].transform;
                return true;
            }
        }
        player = null;
        return false;
    }
}
