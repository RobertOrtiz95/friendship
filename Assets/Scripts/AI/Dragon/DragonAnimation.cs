﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAnimation : MonoBehaviour
{
    public Animator anim = null;

    public void Fire()
    {
        anim.SetTrigger("Attack");
    }

    public void SetFlying(bool flying)
    {
        anim.SetBool("flying", flying);
    }
}
