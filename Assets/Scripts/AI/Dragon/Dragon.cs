﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dragon : MonoBehaviour
{
    private enum DragonState
    {
        IDLE,
        STUNNED,
        AERIALATTACK,
        FRONTALATTACK
    }

    private DragonState dragonState = DragonState.IDLE;
    private float shootTimer = 0;
    public float movementSpeed = 1.0f;

    private int shotCount = 5;
    private int currCount = 0;

    private DragonBehaviour dragonAi = null;
    private DragonAnimation dragonAnim = null;
    private DragonShoot dragonFire = null;
    public EnemyStats dragonStats = new EnemyStats(100, 30);
    public Slider healthDisplay = null;

    [Header("dragon positions")]
    public Transform bulletSpawn = null;
    public Transform bulletSpawnFlyBy = null;
    
    //positions
    public Transform leftPos = null;
    public Transform rightPos = null;
    public Transform landLeft_Pos = null;
    public Transform landRight_Pos = null;

    private bool isLeft = false;

    private int aerialStep = 0;

    private void Start()
    {
        dragonAi = GetComponent<DragonBehaviour>();
        dragonAnim = GetComponent<DragonAnimation>();
        dragonFire = GetComponent<DragonShoot>();
        healthDisplay.maxValue = 100;
        healthDisplay.value = dragonStats.currHealth;
    }

    void Update ()
    {
        if (dragonAi.CheckForPlayer())
            healthDisplay.gameObject.SetActive(true);
        else
            healthDisplay.gameObject.SetActive(false);

        if (dragonStats.isDead)
            return;
	    switch(dragonState)
        {
            case DragonState.IDLE:
                Idle();
                break;
            case DragonState.STUNNED:
                Stunned();
                break;
            case DragonState.AERIALATTACK:
                AerialAttackLeft();
                break;
            case DragonState.FRONTALATTACK:
                FrontalAttack();
                break;
        }
	}

    private void Idle()
    {
        if (!dragonAi.CheckForPlayer())
            return;

        if (dragonAi.Idle())
        {
            dragonAi.UpdateLandingPos();
            dragonAnim.SetFlying(true);
            int rando = Random.Range(0, 3);
            dragonState = (DragonState)rando;
            Debug.Log(dragonState);
        }
    }

    private void Stunned()
    {
        if (dragonAi.MoveToWithPlayerTarget(landRight_Pos.position, movementSpeed))
            if (dragonAi.Idle())
                dragonState = DragonState.FRONTALATTACK;
    }

    private void FrontalAttack()
    {
        if (!dragonAi.DragonLand(movementSpeed))
            return;

        if (shootTimer < 0)
        {
            dragonFire.GetFireBall(bulletSpawn.position, bulletSpawn.rotation);
            currCount++;
            shootTimer = 2;
        }

        shootTimer -= Time.deltaTime;
        
        if(currCount >= shotCount)
        {
            currCount = 0;
            dragonState = DragonState.AERIALATTACK;
        }
    }

    public void Dead()
    {
        this.gameObject.SetActive(false);
        GameManager.instance.DragonDead();
        dragonStats.ResetHealth();
    }

    public void Damage(int dmg)
    {
        dragonStats.currHealth -= dmg;
        healthDisplay.value = dragonStats.currHealth;

        if (dragonStats.currHealth < 0)
        {
            dragonStats.isDead = true;
            dragonAnim.anim.SetBool("IsDead", dragonStats.isDead);
        }
    }

    private void AerialAttackLeft()
    {
        if (isLeft)
        {
            switch (aerialStep)
            {
                case 0:
                    if (dragonAi.MoveToWithPlayerTarget(leftPos.position, movementSpeed))
                        aerialStep++;
                    break;
                case 1:
                    if (dragonAi.MoveToWithPlayerTarget(rightPos.position, movementSpeed))
                        aerialStep++;

                    if (shootTimer == .5f)
                        dragonFire.GetFireBall(bulletSpawnFlyBy.position, bulletSpawnFlyBy.rotation);
                    shootTimer -= Time.deltaTime;
                    if (shootTimer < 0)
                        shootTimer = .5f;
                    break;
                case 2:
                    if (dragonAi.MoveToWithPlayerTarget(landRight_Pos.position, movementSpeed))
                    {
                        dragonState = DragonState.IDLE;
                        dragonAnim.SetFlying(false);
                        aerialStep = 0;
                        isLeft = false;
                    }
                    break;
            }
        }
        else
        {
            switch (aerialStep)
            {
                case 0:
                    if(dragonAi.MoveToWithPlayerTarget(rightPos.position, movementSpeed))
                        aerialStep++;                
                    break;
                case 1:
                    if (dragonAi.MoveToWithPlayerTarget(leftPos.position, movementSpeed))
                        aerialStep++;

                    if(shootTimer == .5f)
                        dragonFire.GetFireBall(bulletSpawnFlyBy.position, bulletSpawnFlyBy.rotation);
                    shootTimer -= Time.deltaTime;
                    if (shootTimer < 0)
                        shootTimer = .5f;
                    break;
                case 2:
                    if (dragonAi.MoveToWithPlayerTarget(landLeft_Pos.position, movementSpeed))
                    {
                        dragonState = DragonState.IDLE;
                        dragonAnim.SetFlying(false);
                        aerialStep = 0;
                        isLeft = true;
                    }
                    break;
            }
        }
    }    
}
