﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonShoot : MonoBehaviour
{
    [SerializeField]
    private EnemyProjectile fireBall = null;
    private List<EnemyProjectile> fireBalls = new List<EnemyProjectile>();
    public Transform fileBallParent = null;

    private void Start()
    {
        CreatePool();
    }

    public EnemyProjectile GetFireBall(Vector3 position, Quaternion rot)
    {
        for(int i = 0; i < fireBalls.Count; i++)
        {
            if (!fireBalls[i].gameObject.activeInHierarchy)
            {
                fireBalls[i].transform.position = position;
                fireBalls[i].transform.rotation = rot;
                fireBalls[i].gameObject.SetActive(true);
                return fireBalls[i];
            }
        }
        return null;
    }

    private void CreatePool()
    {
        for(int i = 0; i < 10; i++)
        {
            GameObject go = Instantiate(fireBall.gameObject, fileBallParent);
            go.SetActive(false);
            fireBalls.Add(go.GetComponent<EnemyProjectile>());
        }
    }

}
