﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : MonoBehaviour
{
    private enum Behaviour
    {
        IDLE,
        WALKING,
        AGGRO
    }
    public EnemyStats goblinStats = new EnemyStats(100, 5);
    public float walkSpeed = 2.0f;
    public float runSpeed = 5.0f;
    private GoblinBehaviour goblinAi = null;
    private GablinWeaponFrames gWFrames = null;
    public EnemyWeapon enemyWeapon = null;
    private Animator anim = null;
    private float direct = 0.0f;

    private bool hit = false;
    public void IsHit() {
        hit = true;
        anim.SetTrigger("IsHit");
    }
    public void NotHit() { Debug.Log("No Longer Hit"); hit = false; }

    public float timeBetweenAttacks = 2.0f;
    private float attackTimer = 0;

    private Behaviour currentBehaviour = Behaviour.IDLE; 

    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        goblinAi = this.GetComponent<GoblinBehaviour>();
        gWFrames = GetComponent<GablinWeaponFrames>();
        enemyWeapon.damage = goblinStats.damage;
    }
    
    private void FixedUpdate()
    {
        if (goblinStats.isDead)
            return;
        if (hit)
            return;

        switch (currentBehaviour)
        {
            case Behaviour.IDLE:
                Idle();
                break;
            case Behaviour.WALKING:
                Walk();
                break;
            case Behaviour.AGGRO:
                Attack();
                break;
        }
    }

    private void Attack()
    {
        if (!goblinAi.PlayerInMyArea() || goblinAi.GetPlayer().GetDead())
        {
            currentBehaviour = Behaviour.IDLE;
            return;
        }

        if (goblinAi.GetPlayerPos().position.x < this.transform.position.x)
            direct = -1.0f;
        if (goblinAi.GetPlayerPos().position.x > this.transform.position.x)
            direct = 1.0f;

        if (CheckAttackRange())
        {
            anim.SetFloat("Speed", 0);
            AttackTimer();
        }
        else
        {
            goblinAi.Move(direct, runSpeed);
            anim.SetFloat("Speed", runSpeed);
        }
    }

    private void AttackTimer()
    {
        if (attackTimer == 0)
        {
            anim.SetTrigger("Attack");
            gWFrames.DeactivateAllFrames();
        }

        attackTimer += Time.deltaTime;

        if(attackTimer > timeBetweenAttacks)
        {
            attackTimer = 0;
        }
    }

    private bool CheckAttackRange()
    {
        if (Vector3.Distance(transform.position, goblinAi.GetPlayerPos().position) < .5)
            return true;
        return false;
    }

    private void Walk()
    {
        gWFrames.DeactivateAllFrames();
        if (goblinAi.PlayerInMyArea() && !goblinAi.GetPlayer().GetDead())
            currentBehaviour = Behaviour.AGGRO;

        if (goblinAi.GetPosWalk() > transform.position.x)
            direct = 1.0f;
        else
            direct = -1.0f;

        goblinAi.Move(direct, walkSpeed);
        anim.SetFloat("Speed", walkSpeed);

        if (this.transform.position.x < goblinAi.GetPosWalk() + 1 && this.transform.position.x > goblinAi.GetPosWalk() - 1)
            currentBehaviour = Behaviour.IDLE;
    }

    private void Idle()
    {
        gWFrames.DeactivateAllFrames();

        if (goblinAi.PlayerInMyArea())
            currentBehaviour = Behaviour.AGGRO;
        anim.SetFloat("Speed", 0);
        if(goblinAi.IdleTimer())
        {
            goblinAi.UpdatePosWalk();
            currentBehaviour = Behaviour.WALKING;
        }
    }
    public void Dead()
    {
        this.gameObject.SetActive(false);
        goblinStats.ResetHealth();
    }

    public void Damage(int dmg)
    {
        goblinStats.currHealth -= dmg;

        if (goblinStats.currHealth < 0)
        {
            Debug.Log("Goblin.cs::Damage()");
            goblinStats.isDead = true;
            anim.SetBool("Dead", goblinStats.isDead);
        }
    }
}
