﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GablinWeaponFrames : MonoBehaviour
{
    public GameObject frame1, frame2, frame3;

    public void DeactivateAllFrames()
    {
        frame1.SetActive(false);
        frame2.SetActive(false);
        frame3.SetActive(false);
    }
    public void ActivateFrame1()
    {
        DeactivateAllFrames();
        frame1.SetActive(true);
    }
    public void ActivateFrame2()
    {
        DeactivateAllFrames();
        frame2.SetActive(true);
    }
    public void ActivateFrame3()
    {
        DeactivateAllFrames();
        frame3.SetActive(true);
    }

}
