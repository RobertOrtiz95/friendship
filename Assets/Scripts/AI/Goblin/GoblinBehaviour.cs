﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class GoblinBehaviour : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private Vector2 moveDirection = Vector2.zero;
    [Tooltip("Required to have a designated walking area")]
    public Collider2D walkingArea = null;
    private Player target = null;
    public Transform GetPlayerPos() { return target.transform; }

    private Vector3 faceLeft = new Vector3(0, 180, 0);
    private float posWalk = 0;
    private float idleTimer = 3.0f;

    public float GetPosWalk()
    {
        return posWalk;//Random.Range(walkingArea.bounds.min.x, walkingArea.bounds.max.x);
    }

    public float UpdatePosWalk()
    {
        posWalk = Random.Range(walkingArea.bounds.min.x, walkingArea.bounds.max.x);
        return posWalk;
    }

    private void OnEnable()
    {
        rb2d = this.GetComponent<Rigidbody2D>();
    }
    
    public bool IdleTimer()
    {
        idleTimer -= Time.deltaTime;
        if(idleTimer <= 0)
        {
            idleTimer = Random.Range(3.0f, 5.0f);
            return true;
        }
        return false;
    }

    public void Move(float direct, float speed)
    {
        if (direct < 0)
            transform.rotation = Quaternion.Euler(faceLeft);
        if (direct > 0)
            transform.rotation = Quaternion.Euler(Vector3.zero);

        moveDirection.x = direct * speed;
        moveDirection.y = rb2d.velocity.y;
        rb2d.velocity = moveDirection;
    }

    public Player GetPlayer()
    {
        return target;
    }

    public bool PlayerInMyArea()
    {
        Collider2D[] colls = Physics2D.OverlapBoxAll(walkingArea.transform.position, walkingArea.bounds.size, .5f);
        for(int i = 0; i < colls.Length; i++)
        {
            if (colls[i].gameObject.tag == "Player")
            {
                target = colls[i].GetComponent<Player>();
                return true;
            }
        }
        target = null;
        return false;
    }

}
