﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SpiderBehaviour : MonoBehaviour
{
    public Collider2D spiderArea = null;
    private Rigidbody2D rb2d = null;

    private Vector2 moveDirection = Vector2.zero;
    private Transform target = null;
    public Vector2 GetPlayerPos() { return target.position; }

    private Vector3 faceLeft = new Vector3(0, 180, 0);
    private float posWalk = 0;
    private float idleTimer = 3.0f;

    public float GetPosWalk() {
        return posWalk;
    }

    public float UpdatePosWalk() {
        posWalk = Random.Range(spiderArea.bounds.min.x, spiderArea.bounds.max.x);
        return posWalk;
    }

    private void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    public bool IdleTimer()
    {
        idleTimer -= Time.deltaTime;

        if(idleTimer < 0)
        {
            idleTimer = Random.Range(1, 3);
            return true;
        }

        return false;
    }

    public void Move(float direct, float speed)
    {
        if (direct < 0)
            transform.rotation = Quaternion.Euler(faceLeft);
        else
            transform.rotation = Quaternion.Euler(Vector3.zero);

        moveDirection.x = direct * speed;
        moveDirection.y = rb2d.velocity.y;
        rb2d.velocity = moveDirection;
    }

    public bool PlayerInMyArea()
    {
        Collider2D[] colls = Physics2D.OverlapBoxAll(spiderArea.transform.position, spiderArea.bounds.size, .5f);
        for(int i = 0; i < colls.Length; i++)
        {
            if(colls[i].gameObject.tag == "Player")
            {
                target = colls[i].transform;
                return true;
            }
        }
        target = null;
        return false;
    }
}
