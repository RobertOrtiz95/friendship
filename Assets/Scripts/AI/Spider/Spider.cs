﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MonoBehaviour
{
    private enum SpiderStates
    {
        IDLE,
        WALKING,
        AGGRO
    }
    public EnemyStats spiderStats = new EnemyStats(30, 3);
    public float walkSpeed = 1.0f;
    public float runSpeed = 2.0f;
    private SpiderBehaviour spiderAi = null;
    private Animator anim = null;

    private float direct = 0.0f;
    private bool hit = false;
    public void IsHit() {
        hit = true;
        anim.SetTrigger("IsHit");
    }
    public void NotHit() { hit = false; }

    public float timeBetweenShots = 2.0f;
    private float attackTimer = 0;
    private SpiderShoot spiderShoot = null;
    [Header("Spider Shooting requirements")]
    public Transform poisonSpawnPoint = null;


    private SpiderStates currBehaviour = SpiderStates.IDLE;

    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        spiderAi = GetComponent<SpiderBehaviour>();
        spiderShoot = GetComponent<SpiderShoot>();
    }

    private void FixedUpdate()
    {
        if (spiderStats.isDead)
            return;
        if (hit)
            return;

        switch (currBehaviour)
        {
            case SpiderStates.AGGRO:
                Attack();
                break;
            case SpiderStates.IDLE:
                Idle();
                break;
            case SpiderStates.WALKING:
                Walk();
                break;
        }
    }

    private void Attack()
    {
        if(!spiderAi.PlayerInMyArea())
        {
            currBehaviour = SpiderStates.IDLE;
            return;
        }

        if (spiderAi.GetPlayerPos().x < transform.position.x)
            direct = -1.0f;
        else
            direct = 1.0f;

        if (CheckAttackRange())
        {
            AttackTimer();
            anim.SetFloat("Speed", 0);
        }
        else
        {
            spiderAi.Move(direct, runSpeed);
            anim.SetFloat("Speed", runSpeed);
        }

        if (CheckAttackRange())
        {
            AttackTimer();
        }        
    }

    public void PoisonSpit()
    {
        spiderShoot.CallVenomBall(poisonSpawnPoint.position, poisonSpawnPoint.rotation);
    }

    private void AttackTimer()
    {
        if (attackTimer == 2)
            anim.SetTrigger("Attack");
        attackTimer -= Time.deltaTime;

        if (attackTimer < 0)
            attackTimer = 2;
    }

    private void Idle()
    {
        if (spiderAi.PlayerInMyArea())
            currBehaviour = SpiderStates.AGGRO;
        //setanim
        anim.SetFloat("Speed", 0);
        if (spiderAi.IdleTimer())
        {
            spiderAi.UpdatePosWalk();
            currBehaviour = SpiderStates.WALKING;
        }
    }

    private void Walk()
    {
        if(spiderAi.PlayerInMyArea())
        {
            currBehaviour = SpiderStates.AGGRO;
        }

        if (spiderAi.GetPosWalk() > transform.position.x)
            direct = 1.0f;
        else
            direct = -1.0f;

        spiderAi.Move(direct, walkSpeed);
        //call walking animation
        anim.SetFloat("Speed", walkSpeed);

        if (transform.position.x < spiderAi.GetPosWalk() + 1 && transform.position.x > spiderAi.GetPosWalk() - 1)
            currBehaviour = SpiderStates.IDLE;
    }

    private bool CheckAttackRange()
    {
        if (Vector3.Distance(this.transform.position, spiderAi.GetPlayerPos()) < 1)
            return true;
        return false;
    }

    public void Damage(int dmg)
    {
        spiderStats.currHealth -= dmg;

        if(spiderStats.currHealth < 0)
        {
            spiderStats.isDead = true;
            anim.SetBool("IsDead", spiderStats.isDead);
        }
    }

    public void Dead()
    {
        gameObject.SetActive(false);
        spiderStats.ResetHealth();
    }
}
