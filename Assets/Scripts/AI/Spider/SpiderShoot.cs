﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderShoot : MonoBehaviour {
    private int ammoCount = 10;
    public GameObject venomBall = null;
    private List<GameObject> venomBalls = new List<GameObject>();

    private void OnEnable()
    {
        CreatePool();
    }

    private void CreatePool()
    {
        for(int i = 0; i < ammoCount; i++)
        {
            GameObject vB = Instantiate(venomBall);
            vB.SetActive(false);
            venomBalls.Add(vB);
        }

        for (int i = 0; i < venomBalls.Count; i++)
            venomBalls[i].transform.SetParent(venomBall.transform);
    }

    public GameObject CallVenomBall(Vector3 pos, Quaternion rot)
    {
        for(int i = 0; i < venomBalls.Count; i++)
            if(!venomBalls[i].activeInHierarchy)
            {
                venomBalls[i].transform.position = pos;
                venomBalls[i].transform.rotation = rot;
                venomBalls[i].SetActive(true);
                return venomBalls[i];
            }
        return null;
    }
}
