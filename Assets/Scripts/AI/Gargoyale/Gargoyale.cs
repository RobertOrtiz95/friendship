﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gargoyale : MonoBehaviour
{
    private enum GargoyaleState
    {
        LANDED,
        FLYINGTOTARGET,
        FLYINGIDLE,
        AGGRO
    }
    public EnemyStats gargoyalStats = new EnemyStats(3, 10);

    public GargoyaleBehaviour gargoyalAi = null;
    public float movementSpeed = 1.0f;
    private Animator anim = null;
    public float flightSpeed = 1.0f;
    private bool isFlying = false;
    private Vector2 target = Vector2.zero;
    private GargoyaleState currState = GargoyaleState.LANDED;

    [Header("shooting requirements")]
    public GargoyaleShoot gargoyalShoot = null;
    public Transform boulderSpawnPoint = null;
    public Transform fireBallTransform = null;
    
    private float attackTimer = 2.0f;

    private bool hit = false;
    public void IsHit()
    {
        hit = true;
        anim.SetTrigger("IsHit");
    }
    public void NotHit() { hit = false; }


    private void OnEnable()
    {
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (hit || gargoyalStats.isDead)
            return;

        switch(currState)
        {
            case GargoyaleState.AGGRO:
                Attack();
                break;
            case GargoyaleState.FLYINGIDLE:
                FlyingIdle();
                break;
            case GargoyaleState.FLYINGTOTARGET:
                FlyingTarget();
                break;
            case GargoyaleState.LANDED:
                Landed();
                break;
        }
    }

    private void Landed()
    {        
        if (gargoyalAi.Idle())
        {
            target = gargoyalAi.UpdateFlyToPos();
            currState = GargoyaleState.FLYINGTOTARGET;
            isFlying = true;
            anim.SetBool("isFlying", isFlying);
        }
    }
    
    private void Attack()
    {
        if (!gargoyalAi.CheckForPlayer())
        {
            currState = GargoyaleState.FLYINGIDLE;
            return;
        }

        target = gargoyalAi.GetPlayer();
        
        if (Vector2.Distance(transform.position, target) > 1)
            gargoyalAi.Move(target, movementSpeed);

        if (target.y < transform.position.y + .5f && target.y > transform.position.y - .5f)
            AttackTimer("FireAttack");
        else
            AttackTimer("BoulderThrow");
    }

    private void AttackTimer(string attackName)
    {
        if (attackTimer == 2)
            anim.SetTrigger(attackName);
        attackTimer -= Time.deltaTime;

        if(attackTimer < 0)
        {
            attackTimer = 2;
        }
    }

    //public functions for the animations
    public void FireAttack()
    {
        gargoyalShoot.GetFireBall(fireBallTransform.position, fireBallTransform.rotation).damage = gargoyalStats.damage;
    }
    public void BoulderAttack()
    {
        gargoyalShoot.GetBoulder(boulderSpawnPoint.position, boulderSpawnPoint.rotation).damage = gargoyalStats.damage;
    }

    private void FlyingIdle()
    {
        attackTimer = 0;
        if(gargoyalAi.CheckForPlayer())
        {
            currState = GargoyaleState.AGGRO;
            return;
        }

        if(gargoyalAi.Idle())
        {
            target = gargoyalAi.UpdateFlyToPos();
            currState = GargoyaleState.FLYINGTOTARGET;
        }
    }

    private void FlyingTarget()
    { 
        if(gargoyalAi.CheckForPlayer())
        {
            currState = GargoyaleState.AGGRO;
            return;
        }

        gargoyalAi.Move(target, movementSpeed);
        if (Vector2.Distance(transform.position, target) < 1.0f)
            currState = GargoyaleState.FLYINGIDLE;
    }

    public void Damage(int dmg)
    {
        gargoyalStats.currHealth -= dmg;
        
        if(gargoyalStats.currHealth <= 0)
        {
            gargoyalStats.isDead = true;
            anim.SetBool("IsDead", gargoyalStats.isDead);
        }
    }

    public void Dead()
    {
        this.gameObject.SetActive(false);
        gargoyalStats.ResetHealth();
    }
}
