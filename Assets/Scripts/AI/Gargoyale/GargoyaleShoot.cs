﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GargoyaleShoot : MonoBehaviour
{
    private int ammoCount = 10;

    public Transform fireBallParent = null;
    public EnemyProjectile fireBall = null;
    private List<EnemyProjectile> fireBalls = new List<EnemyProjectile>();

    public GameObject boulder_Frame1 = null;
    public GameObject boulder_Frame2 = null;

    public Transform boulderParent = null;
    public EnemyProjectile boulder = null;
    private List<EnemyProjectile> boulders = new List<EnemyProjectile>();

    private void Start()
    {
        CreatePool();
    }

    public void BoulderFrame1()
    {
        DeactivateAll();
        boulder_Frame1.SetActive(true);
    }

    public void DeactivateAll()
    {
        boulder_Frame1.SetActive(false);
        boulder_Frame2.SetActive(false);
    }

    public void BoulderFrame2()
    {
        DeactivateAll();
        boulder_Frame2.SetActive(true);
    }

    public EnemyProjectile GetFireBall(Vector3 pos, Quaternion rot)
    {
        for(int i = 0; i < fireBalls.Count; i++)
        {
            if(!fireBalls[i].gameObject.activeInHierarchy)
            {
                fireBalls[i].transform.position = pos;
                fireBalls[i].transform.rotation = rot;
                fireBalls[i].gameObject.SetActive(true);
                return fireBalls[i];
            }
        }

        Debug.LogWarning("Warning : GargoyalShoot.cs::GetFireBall() Not enough fireballs in pool");
        return null;
    }

    public EnemyProjectile GetBoulder(Vector2 pos, Quaternion rot)
    {
        for(int i = 0; i < boulders.Count; i++)
        {
            if(!boulders[i].gameObject.activeInHierarchy)
            {
                boulders[i].transform.position = pos;
                boulders[i].transform.rotation = rot;
                boulders[i].gameObject.SetActive(true);
                return boulders[i];
            }
        }
        return null;
    }
    
    private void CreatePool()
    {

        for(int i = 0; i < ammoCount; i++)
        {
            GameObject fb = Instantiate(fireBall.gameObject, fireBallParent);
            fb.SetActive(false);
            fireBalls.Add(fb.GetComponent<EnemyProjectile>());
            GameObject bo = Instantiate(boulder.gameObject, boulderParent);
            bo.SetActive(false);
            boulders.Add(bo.GetComponent<EnemyProjectile>());
        }
        
    }
}
