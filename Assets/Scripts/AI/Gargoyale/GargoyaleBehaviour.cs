﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class GargoyaleBehaviour : MonoBehaviour
{
    public Collider2D flyingArea = null;

    private Rigidbody2D rb2d = null;
    private Vector2 leftFacing = new Vector2(0, 180);
    private Transform player = null;
    public Vector2 GetPlayer() { return player.position; }

    private float idleTimer = 3.0f;
    private float damagedTimer = 2.0f;

    private void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// this function is the function that allows the gargoyal to move
    /// </summary>
    /// <param name="target"></param>
    /// <param name="speed"></param>
    public void Move(Vector2 target, float speed)
    {
        rb2d.velocity = Vector3.zero;
        rb2d.gravityScale = 0;

        if (target.x < transform.position.x)
            transform.rotation = Quaternion.Euler(leftFacing);
        if (target.x > transform.position.x)
            transform.rotation = Quaternion.Euler(Vector2.zero);

        rb2d.position = Vector2.Lerp(rb2d.position, target, speed * Time.deltaTime);
    }

    /// <summary>
    /// gets the the random position in which the Gargoyale is going to fly towards
    /// </summary>
    /// <returns>returns the positoin as a Vector2</returns>
    public Vector2 UpdateFlyToPos()
    {
        Vector3 temp = Vector2.zero;
        temp.x = Random.Range(flyingArea.bounds.min.x, flyingArea.bounds.max.x);
        temp.y = Random.Range(flyingArea.bounds.min.y, flyingArea.bounds.max.y);
        return temp;
    }
    
    /// <summary>
    /// checks to see if the player is in bounds of the designated flying area
    /// </summary>
    /// <returns>true if player is in bounds false if not</returns>
    public bool CheckForPlayer()
    {
        Collider2D[] colls = Physics2D.OverlapBoxAll(flyingArea.transform.position, flyingArea.bounds.size, .5f);
        for(int i = 0; i < colls.Length; i++)
        {
            if (colls[i].tag == "Player")
            {
                player = colls[i].transform;
                return true;
            }
        }
        player = null;
        return false;
    }

    /// <summary>
    /// once hit this function counts down until the gargoyale will be able to fly again.
    /// </summary>
    /// <returns></returns>
    public bool DamageTimer()
    {
        rb2d.gravityScale = 1;
        damagedTimer -= Time.deltaTime;
        if (damagedTimer <= 0)
        {
            damagedTimer = 2.0f;
            rb2d.gravityScale = 0;
            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// amount of time the gargoyal will remain idle
    /// </summary>
    /// <returns></returns>
    public bool Idle()
    {
        idleTimer -= Time.deltaTime;

        if (idleTimer <= 0)
        {
            idleTimer = Random.Range(1, 3);
            return true;
        }
        else
            return false;
    }
}
