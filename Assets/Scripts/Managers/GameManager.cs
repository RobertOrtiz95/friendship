﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;

    private void OnEnable()
    {
        instance = this;
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void DragonDead()
    {
        Debug.Log("dragon is dead. TODO: create a win screen");
    }

    public void RestartScene()
    {
        SceneManager.LoadScene("LevelTest");
    }
}
