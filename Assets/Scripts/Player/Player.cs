﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private int baseHealth = 3;
    public int currHealth = 3;
    public int weaponDamage = 5;
    public int beamDamage = 10;

    private bool isDead = false;
    public void SetDead(bool Dead) {
        isDead = Dead;
        playerUI.ActivateGameOver();
    }
    public bool GetDead() { return isDead; }
    public float movementSpeed = 4.0f;
    public float jumpHeight = 10.0f;
    public MyCharacterController m_CharacterController = null;
    public PlayerAnimation p_Animation = null;
    [Header("Shooting Properties")]
    public PlayerShoot playerShoot = null;
    public Transform spawnPoint = null;
    public PlayerUI playerUI = null;
    public WeaponBehaviour weapon = null;

    private bool isHit = false;

    public void SetHit() { isHit = false; }

    private void OnEnable()
    {
        isDead = false;
        currHealth = baseHealth;
    }

    private void Start()
    {
        weapon.damage = weaponDamage;
    }

    public void Shoot()
    {
        playerShoot.CallBullet(spawnPoint.position, spawnPoint.rotation);
    }

    public void ResetPlayer()
    {
        currHealth = baseHealth;
        isDead = false;
    }

    private void FixedUpdate()
    {
        if (isHit || isDead)
            return;

        m_CharacterController.Move(movementSpeed, jumpHeight);

        p_Animation.anim.SetFloat("Speed", m_CharacterController.GetXAbs());
        p_Animation.anim.SetBool("IsGrounded", m_CharacterController.isGrounded);

        if (Input.GetButtonDown("Fire1"))
            p_Animation.anim.SetTrigger("Attack");
        if (Input.GetButtonDown("Fire2") && m_CharacterController.isGrounded)
            p_Animation.anim.SetTrigger("Beam");
        else if (Input.GetButtonDown("Fire2") && !m_CharacterController.isGrounded)
            playerShoot.CallBullet(spawnPoint.position, spawnPoint.rotation).damage = beamDamage;
    }
    public void Damage(int dmg)
    {
        currHealth -= dmg;
        p_Animation.anim.SetTrigger("IsHit");
        isHit = true;
        playerUI.UpdateHealth(currHealth);
        if(currHealth == 0)
        {
            isDead = true;
            p_Animation.anim.SetBool("IsDead", isDead);
        }
    }
}
