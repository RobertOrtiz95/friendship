﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBehaviour : MonoBehaviour
{
    public int damage = 0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Goblin":
                Goblin g = collision.GetComponent<Goblin>();
                g.IsHit();
                g.Damage(damage);
                g.GetComponent<Rigidbody2D>().AddForce(transform.right * 50);
                break;
            case "Gargoyal":
                Gargoyale gargoyal = collision.GetComponent<Gargoyale>();
                gargoyal.IsHit();
                gargoyal.Damage(damage);
                gargoyal.GetComponent<Rigidbody2D>().AddForce(transform.right * 50);
                break;
            case "Spider":
                Spider spider = collision.GetComponent<Spider>();
                spider.IsHit();
                spider.Damage(damage);
                spider.GetComponent<Rigidbody2D>().AddForce(transform.right * 50);
                break;
            case "Boss":
                Dragon dragon = collision.GetComponent<Dragon>();
                dragon.Damage(damage);
                break;
        }
    }
}
