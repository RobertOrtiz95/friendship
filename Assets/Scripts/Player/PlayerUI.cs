﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public Image[] healthImages = null;
    public GameObject gameOverPanel = null;

    public void UpdateHealth(int CurrentHealth)
    {
        Debug.Log("Current Health : " + CurrentHealth);
        if (CurrentHealth < 0)
        {
            gameOverPanel.SetActive(true);
            healthImages[0].gameObject.SetActive(false);
            return;
        }
        healthImages[CurrentHealth].gameObject.SetActive(false);
    }

    public void ActivateGameOver()
    {
        gameOverPanel.SetActive(true);
    }
}
