﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour {
    public int damage = 0;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Goblin":
                Goblin g = collision.transform.GetComponent<Goblin>();
                g.IsHit();
                g.Damage(damage);
                g.GetComponent<Rigidbody2D>().AddForce(transform.right * 50);
                break;
            case "Gargoyal":
                Gargoyale gargoyal = collision.transform.GetComponent<Gargoyale>();
                gargoyal.IsHit();
                gargoyal.Damage(damage);
                gargoyal.GetComponent<Rigidbody2D>().AddForce(transform.right * 50);
                break;
            case "Spider":
                Spider spider = collision.transform.GetComponent<Spider>();
                spider.IsHit();
                spider.Damage(damage);
                spider.GetComponent<Rigidbody2D>().AddForce(transform.right * 50);
                break;
            case "Boss":
                Dragon dragon = collision.transform.GetComponent<Dragon>();
                dragon.Damage(damage);
                break;
        }
    }
}
