﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MyCharacterController : MonoBehaviour
{
    public Rigidbody2D rb2D = null;
    public Transform groundCheck = null;

    [SerializeField]
    public bool isGrounded = true;
    private float x = 0;
    public float GetXAbs() { return Mathf.Abs(x); }

    private bool attacking = false;
    public bool GetAttacking() { return attacking; }
    public void SetAttacking() { attacking = false; }

    private void OnEnable()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        isGrounded = GroundCheck();        
    }

    private bool GroundCheck()
    {
        Collider2D[] colls = Physics2D.OverlapCircleAll(groundCheck.position, .05f);

        for(int i = 0; i < colls.Length; i++)
        {
            if (colls[i].gameObject != gameObject && colls[i].gameObject.tag == "Ground")
                return true;
        }

        return false;
    }

    public void Move(float Speed, float force)
    {
        x = Input.GetAxisRaw("Horizontal");

        if (x < 0)
            this.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 180f, 0));
        if (x > 0)
            this.gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);

        rb2D.velocity = new Vector2(x * Speed, rb2D.velocity.y);

        if (Input.GetButtonDown("Jump") && isGrounded)
            Jump(force);
    }

    private void Jump(float force)
    {
        isGrounded = false;
        rb2D.AddForce(new Vector2(rb2D.velocity.x, force));
    }
}
