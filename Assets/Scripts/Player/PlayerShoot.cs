﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public PlayerBullet projectile = null;
    public Transform projectileParent = null;
    private List<PlayerBullet> projectilePool = new List<PlayerBullet>();

    private void OnEnable()
    {
        CreatePool();
    }
    
    private void ClearPool()
    {
        for(int i = 0; i < projectilePool.Count; i++)
            Destroy(projectilePool[i]);

        projectilePool.Clear();
    }

    public PlayerBullet CallBullet(Vector3 pos, Quaternion rot)
    {
        for(int i = 0; i < projectilePool.Count; i++)
        {
            if(!projectilePool[i].gameObject.activeInHierarchy)
            {
                projectilePool[i].transform.position = pos;
                projectilePool[i].transform.rotation = rot;
                projectilePool[i].gameObject.SetActive(true);
                return projectilePool[i];
            }
        }
        return null;
    }

    private void CreatePool()
    {
        for(int i = 0; i < 10; i++)
        {
            GameObject go = Instantiate(projectile.gameObject, projectileParent);
            go.SetActive(false);
            projectilePool.Add(go.GetComponent<PlayerBullet>());
        }
    }
}
