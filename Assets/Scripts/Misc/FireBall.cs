﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FireBall : MonoBehaviour
{
    private Rigidbody2D rb2d = null;

    private void OnEnable()
    {
        rb2d = this.GetComponent<Rigidbody2D>();
        rb2d.AddForce(transform.right * 100);
    }
}
