﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallAnimation : MonoBehaviour
{
    private Animator anim = null;

    private void OnEnable()
    {
        anim = this.GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        anim.SetTrigger("OnImpact");
    }

    public void Disappear()
    {
        this.gameObject.SetActive(false);
    }

}
