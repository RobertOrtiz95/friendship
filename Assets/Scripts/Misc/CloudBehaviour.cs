﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudBehaviour : MonoBehaviour
{
    public float movementSpeed = 1.0f;
    private const float maxDistance = -5.12f;
    private Vector3 move = Vector3.zero;
    
    private void Update()
    {
        MoveLeft();
    }

    private void MoveLeft()
    {
        if (transform.position.x < maxDistance)
            transform.position = Vector3.zero;
        move.x = movementSpeed;
        transform.position -= move * Time.deltaTime;
    }
}
